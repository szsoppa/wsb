class PostsController < ApplicationController
  before_action :find_post, only: %i[show edit update destroy]
  before_action :check_permission, only: %i[edit update destroy]
  skip_before_action :authenticate_user!, only: %i[index show]

  def index
    @posts = Post.all
  end

  def new
    @post = Post.new
  end

  def show
  end

  def edit
  end

  def update
    if @post.update(permitted_params)
      redirect_to @post
    else
      render :edit
    end
  end

  def create
    @post = current_user.posts.new(permitted_params)
    if @post.save
      redirect_to @post
    else
      render :new
    end
  end

  def destroy
    @post.destroy
    redirect_to posts_path
  end

  private

  def find_post
    @post = Post.find(params[:id])
  end

  def check_permission
    redirect_to posts_path if @post.user != current_user
  end

  def permitted_params
    params.require(:post).permit(:title, :content)
  end
end
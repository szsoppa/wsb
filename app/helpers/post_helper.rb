module PostHelper
  def is_post_author?(post)
    post.user == current_user
  end
end